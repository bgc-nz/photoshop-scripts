# Photoshop-Scripts
A collection of Photoshop scripts I've written for work, and just for fun. Tested on Windows 11 with Photoshop 2024.

## Authors
Ben Cottrell

## License
MIT

## Installation
### Windows
Copy all from Scripts into `C:\Program Files\Adobe\Adobe Photoshop 2024\Presets\Scripts`
### macOS
Copy all from Scripts into `/Applications/Photoshop CC/Presets/Scripts`

## Development Notes
- It took a while for me to discover the *ExtendScript* toolkit, which makes debugging much easier. The tool has built-in basic documentation for the properties and methods for Photoshop and the core, although Adobe has more guiding information around those API's in this PDF: https://github.com/Adobe-CEP/CEP-Resources/blob/master/Documentation/Product%20specific%20Documentation/Photoshop%20Scripting/photoshop-javascript-ref-2020.pdf

- I haven't tested the scripts on macOS yet, but they should still work.

- I found that the order of points used for rectangles 
differs between API functions.

- Single step debugging sometimes didn't work (hence the `writeln` calls)



