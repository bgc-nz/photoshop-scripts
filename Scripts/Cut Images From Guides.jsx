// ***********************
// Cut Guides to Layers
// ***********************
// Author: Ben Cottrell
// Description: Cut images from guides into new documents

// Selection, UnitRect  property indexes
const SEL_TL = 0, SEL_TR = 1, SEL_BR =2, SEL_BL=3;
const UR_L = 0,  UR_T = 1, UR_W=2, UR_H=3;
var originalDocument = null;
var originalLayer = null;
var startRulerUnits = null;
// We duplicate the original document to use as a basis to copy from with separateSelection
// into new documents. This is needed to mitigate the issue of Photoshop running out of memory
var originalDocumentDuplicate = null;

// Transforms the selection box and crops that selection in a new document
function separateSelection(selBox)
{
    // Source selection order is lt, rt, br, bl
    // According to https://stackoverflow.com/a/44066704
    // Expected order is top left, bottom left, bottom right, top right
    //activeDocument = originalDocumentDuplicate;
    activeDocument = originalDocumentDuplicate;
    activeDocument.selection.deselect();
    // Left, Top, Right, Bottom
    var docSelection = Array(
        Array(selBox[SEL_TL][0], selBox[SEL_TL][1]),
        Array(selBox[SEL_TR][0], selBox[SEL_TL][1]),
        Array(selBox[SEL_BR][0], selBox[SEL_BR][1]),
        Array(selBox[SEL_BL][0], selBox[SEL_BL][1])
    );
    activeDocument.selection.select(docSelection);
    activeDocument.selection.copy();
    //activeDocument.copy();
    var destDocWidth = selBox[SEL_TR][0] - selBox[SEL_TL][0];
    var destDocHeight = selBox[SEL_BR][1] - selBox[SEL_TR][1];
    var destDoc = app.documents.add(
        destDocWidth,
        destDocHeight,
        originalDocument.resolution
    );
    activeDocument.paste();
    //activeDocument.paste();
    /*
        //left, top, right, bottom
    activeDocument.crop(Array(
        selBox[SEL_TL][0], selBox[SEL_TL][1],
        selBox[SEL_BR][0], selBox[SEL_BR][1]
    ));*/
}

function main()
{
    if (BridgeTalk.appName != "photoshop") {
		alert("Must run in Photoshop");
		return;
	}
	var activeGuides = activeDocument.guides;
	if (activeGuides.length < 1) {
		alert("Need at least 1 guide");
		return;
	}
    startRulerUnits = app.preferences.rulerUnits;
    app.preferences.rulerUnits = Units.PIXELS;
    originalDocument = activeDocument;
    originalLayer = activeDocument.activeLayer;
    originalDocumentDuplicate = originalDocument.duplicate();
    originalDocumentDuplicate.flatten();
    originalDocumentDuplicate.measurem
    activeDocument = originalDocumentDuplicate;
    var activeLayerBounds = activeDocument.activeLayer.bounds;
	var vGuides = [], hGuides = [];
	for (var gi=0; gi < originalDocumentDuplicate.guides.length; gi++) {
		switch (originalDocumentDuplicate.guides[gi].direction) {
			case Direction.HORIZONTAL:
				hGuides.push(originalDocumentDuplicate.guides[gi].coordinate);
				break;
			case Direction.VERTICAL:
				vGuides.push(originalDocumentDuplicate.guides[gi].coordinate);
				break;
		}
	}
    hGuides.sort();
    vGuides.sort();
    var maxRows = hGuides.length+1;
    var maxCols = vGuides.length+1;
    var selBox = [[0,0],[0,0],[0,0],[0,0]];
    for (var y=0; y < maxRows; y++) {
            selBox = [[0,0],[0,0],[0,0],[0,0]];
            if (y == 0) {
                selBox[SEL_TL][1] = 0;
                selBox[SEL_TR][1] = 0;
                if (maxRows > 1) {
                    selBox[SEL_BR][1] = hGuides[0];
                    selBox[SEL_BL][1] = hGuides[0];
                } else {
                    selBox[SEL_BR][1] = activeLayerBounds[UR_H];
                    selBox[SEL_BL][1] = activeLayerBounds[UR_H];
                }
            } else if (y == maxRows - 1) {
                if (maxRows > 1) {
                    selBox[SEL_TL][1] = hGuides[maxRows - 2];
                    selBox[SEL_TR][1] = hGuides[maxRows - 2];
                } else {
                    selBox[SEL_TL][1] = 0;
                    selBox[SEL_TR][1] = 0;
                }
                selBox[SEL_BR][1] = activeLayerBounds[UR_H];
                selBox[SEL_BL][1] = activeLayerBounds[UR_H];
            } else if (y > 0 && y < maxRows - 1) {
                selBox[SEL_TL][1] = hGuides[y-1];
                selBox[SEL_TR][1] = hGuides[y-1];
                selBox[SEL_BR][1] = hGuides[y];
                selBox[SEL_BL][1] = hGuides[y];
            }
            for (var x=0; x < maxCols; x++) {
                    if (x == 0) {
                        selBox[SEL_TL][0] = 0;
                        if (maxCols > 1) {
                            selBox[SEL_TR][0]  = vGuides[0];
                            selBox[SEL_BR][0] = vGuides[0];
                        } else {
                            selBox[SEL_TR][0] = activeLayerBounds[UR_W];
                            selBox[SEL_BR][0] = activeLayerBounds[UR_W];
                        }
                    } else if (x == maxCols - 1) {
                        if (maxCols > 1) {
                            selBox[SEL_TL][0] = vGuides[maxCols - 2];
                            selBox[SEL_BL][0] = vGuides[maxCols - 2];
                        } else {
                            selBox[SEL_TL][0] = 0;
                            selBox[SEL_BL][0] = 0;
                        }
                        selBox[SEL_TR][0] = activeLayerBounds[UR_W];
                        selBox[SEL_BR][0] = activeLayerBounds[UR_W];
                    } else if (x > 0 && x < maxCols - 1) {
                        selBox[SEL_TL][0] = vGuides[x-1];
                        selBox[SEL_TR][0] = vGuides[x];
                        selBox[SEL_BR][0] = vGuides[x];
                        selBox[SEL_BL][0] = vGuides[x-1];
                    }
                    separateSelection(selBox);
            }
    }
    app.preferences.rulerUnits = startRulerUnits;
}
main();